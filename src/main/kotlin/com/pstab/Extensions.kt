package com.pstab

import com.pstab.Consts.Companion.BLACK
import com.pstab.Consts.Companion.KING
import com.pstab.Consts.Companion.WHITE
import it.unibo.ai.didattica.competition.tablut.domain.Action
import it.unibo.ai.didattica.competition.tablut.domain.Game
import it.unibo.ai.didattica.competition.tablut.domain.State

fun State.getRandomAction(rules: Game): Action {
    val boardSize = board.size
    for (i in 0 until boardSize){
        for (j in 0 until boardSize){
            val figure = getPawn(i, j)
            // add every permitted move
            if (figure.isFigure()) {
                // check horizontal
                var k = 1
                do {
                    val nextPos = i - k
                    if (nextPos >= 0){
                        val action = Action(getBox(i, j), getBox(nextPos, j), turn)
                        if(rules.isActionLegal(this, action))
                            return  action
                    }
                    k++
                }while (nextPos >= 0)
                k = 1
                do {
                    val nextPos = i + k
                    if (nextPos < boardSize){
                        val action = Action(getBox(i, j), getBox(nextPos, j), turn)
                        if(rules.isActionLegal(this, action))
                            return  action
                    }
                    k++
                }while (nextPos < boardSize)

                // check vertical
                k = 1
                do {
                    val nextPos = j - k
                    if (nextPos >= 0){
                        val action = Action(getBox(i, j), getBox(i, nextPos), turn)
                        if(rules.isActionLegal(this, action))
                            return  action
                    }
                    k++
                }while (nextPos >= 0)
                k = 1
                do {
                    val nextPos = j + k
                    if (nextPos < boardSize){
                        val action = Action(getBox(i, j), getBox(i, nextPos), turn)
                        if(rules.isActionLegal(this, action))
                            return  action
                    }
                    k++
                }while (nextPos < boardSize)
            }
        }
    }
    return Action(getBox(0, 0), getBox(0, 0), turn)
}

private fun State.nextActionByNextState(nextState: State): Action? {
    var next: Action? = null
    var from: String? = null
    var to: String? = null

    val boardSize = board.size

    for(i in 0 until boardSize){
        for (j in 0 until boardSize){
            val pawn = getPawn(i, j)
            if(! nextState.getPawn(i, j).equalsPawn(pawn.toString())){
                if (pawn.isFigure()){
                    from = getBox(i, j)
                }else{
                    to = getBox(i, j)
                }
            }
        }
        if (from != null && to != null){
            break
        }
    }

    if (from != null && to != null){
        next = Action(from, to, turn)
    }

    return next
}





fun State.Pawn.isFigure(): Boolean {
    return this.equalsPawn(State.Pawn.BLACK.toString())  || this.equalsPawn(State.Pawn.WHITE.toString()) || this.equalsPawn(
        State.Pawn.KING.toString())
}

data class StateExplorationResult(val action: Action, val state: State)
class Consts{
    companion object{
        const val BLACK = "B"
        const val WHITE = "W"
        const val KING = "K"
        const val WHITE_WIN = "WW"
        const val BLACK_WIN = "BW"
        const val DRAW = "D"
        const val WHITE_PLAYER = "WHITE"
        const val BLACK_PLAYER = "BLACK"

    }
}

fun sigmoid(x: Float): Float{
    return x/20F
}

data class AnalysisResult(val whiteScore: Int, val blackScore : Int, val kingScore : Int){
    companion object{
        const val MAX_ANALYSIS_RES = 10
    }
}
fun cornerAnalysis(state: State, squarePos: Int): AnalysisResult {

    val scores = arrayOf(   intArrayOf(0, 1, 2, 1),
            intArrayOf(1, 2, 3, 2),
            intArrayOf(2, 3, 2, 1 ),
            intArrayOf(1, 2, 1, 0))

    var isKingThere = false
    var isBorder = false
    var blackScore = 0
    var whiteScore = 0
    var kingScore = 0
    val iS : IntProgression
    val jS : IntProgression
    when(squarePos){
        0 -> {
            iS = 0 .. 3
            jS = 0 .. 3
        }
        1 -> {
            iS = 0 .. 3
            jS = 8 downTo 5
        }
        2 -> {
            iS = 8 downTo 5
            jS = 0 .. 3
        }
        3 -> {
            iS = 8 downTo 5
            jS = 8 downTo 5
        }
        else -> return AnalysisResult(0, 0, 0)
    }

    for (i in iS){
        for (j in jS){
            val scoreI = if (i > 4) 8 - i else i
            val scoreJ = if (j > 4) 8 - j else j
            val fig = state.getPawn(i, j)
            when(fig.toString()){
                WHITE -> {
                    // update scores
                    if (!(scoreI == 1 && scoreJ == 3) && !(scoreI == 3 && scoreJ == 1)){
                        var currJ = scoreJ + 1
                        for(a in scoreI until  4) {
                            for (b in currJ until  4){
                                if (scores[a][b] != 0 && !(( a == 0 && b==3 ) || (a ==3 && b==0)))
                                    scores[a][b]--
                            }
                            currJ = 0
                        }
                    }



                }
                BLACK -> {
                    scores[scoreI][scoreJ] *= -1
                    // check for border
                    if (    scores[2][0] + scores[1][1] + scores[0][2] == -6    ||
                            scores[2][1] + scores[1][2] == -6                   ||
                            scores[3][1] + scores[2][2] + scores[1][3] == -6) {
                        blackScore = -AnalysisResult.MAX_ANALYSIS_RES
                        isBorder = true
                    }

                }
                KING -> {
                    isKingThere = true
                    kingScore = AnalysisResult.MAX_ANALYSIS_RES
                    // update scores
                    if (!(scoreI == 1 && scoreJ == 3) && !(scoreI == 3 && scoreJ == 1)){
                        var currJ = scoreJ + 1
                        for(a in scoreI until  4) {
                            for (b in currJ until  4){
                                if (scores[a][b] != 0 && !(( a == 0 && b==3 ) || (a ==3 && b==0)))
                                    scores[a][b]--
                            }
                            currJ = 0
                        }
                    }

                }
                else -> {
                    scores[scoreI][scoreJ] = 0
                }

            }
        }
    }

    if (!isBorder){

        for (a in 0 until 4){
            for (b in 0 until 4){
                if (scores[a][b] > 0) whiteScore += scores[a][b]
                if (scores[a][b] < 0) blackScore += scores[a][b]
            }
        }
        if (!isKingThere){
            kingScore = 0
            //TODO: king distance to the corner
        }

    }

    if (whiteScore > AnalysisResult.MAX_ANALYSIS_RES) whiteScore = AnalysisResult.MAX_ANALYSIS_RES
    if (blackScore < -AnalysisResult.MAX_ANALYSIS_RES) blackScore = -AnalysisResult.MAX_ANALYSIS_RES
    if (kingScore > AnalysisResult.MAX_ANALYSIS_RES) kingScore = AnalysisResult.MAX_ANALYSIS_RES
    if (kingScore < 0) kingScore = 0
    return AnalysisResult(whiteScore, blackScore, kingScore)
}

fun isSameNumberOfFigure(state1: State, state2: State): Boolean{
    var nFig1 = 0
    var nFig2 = 0

    for (i in 0 .. 8){
        for (j in 0..8){
            val fig1 = state1.getPawn(i,j)
            val fig2 = state2.getPawn(i,j)
            if (fig1.isFigure())
                nFig1++
            if (fig2.isFigure())
                nFig2++
        }
    }
    return nFig1 == nFig2
}