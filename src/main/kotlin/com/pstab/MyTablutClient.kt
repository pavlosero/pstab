package com.pstab

import com.pstab.Consts.Companion.BLACK
import com.pstab.Consts.Companion.BLACK_WIN
import com.pstab.Consts.Companion.DRAW
import com.pstab.Consts.Companion.KING
import com.pstab.Consts.Companion.WHITE
import com.pstab.Consts.Companion.WHITE_WIN
import it.unibo.ai.didattica.competition.tablut.client.TablutClient
import it.unibo.ai.didattica.competition.tablut.domain.*
import kotlinx.coroutines.*
import java.io.IOException
import kotlin.system.exitProcess

class MyTablutClient(player: String?, name: String? = "com.pstab.PSTab",val timeout: Int = 60,val ipAddress: String? = "localhost") : TablutClient(player, name, timeout, ipAddress) {

    companion object{
        const val MAX_DEPTH = 3
        const val TIMEOUT = 50000
        const val DEFAULT_VALUE = -100F
        const val WHITE_POINTS = 2
        const val BLACK_POINTS = 1
    }

    private lateinit var mRules: Game
    private var mNextAction : Action? = null
    private val winningPositionsArray = listOf(0, 1, 2, 6, 7, 8)
    private var boardSize = 0

    private var mNWhites = 0
    private var mNBlacks = 0
    private var kinkI = 0
    private var kinkJ = 0

    override fun run() {
        println("$player | $name | $ipAddress | $timeout ")
        try {
            declareName()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        var state = StateTablut() as State
        state.turn = State.Turn.WHITE
        mRules = GameAshtonTablut(99, 0, "garbage", "fake", "fake");
        boardSize = state.board.size

        while (true){
            try {
                read()
            } catch (e1: ClassNotFoundException) {
                // TODO Auto-generated catch block
                e1.printStackTrace()
                exitProcess(1)
            } catch (e1: IOException) {
                e1.printStackTrace()
                exitProcess(1)
            }
            state = currentState

            try {
                Thread.sleep(2000)
            } catch (e: InterruptedException) {

            }
            if(state.turn == player) {

                runBlocking {
                    if (player.toString() == WHITE){
                        mNextAction = shouldKingExit(state)
                        if (mNextAction!= null && !mRules.isActionLegal(state,mNextAction))
                            mNextAction = null
                    }
                    if (mNextAction == null){
                        val bestScore = minMax(state, MAX_DEPTH, -100F, 100F, true)
                        println("$bestScore")

                    }
                    write(mNextAction)
                    mNextAction = null
                }

            }else if (state.turn != player) {
                println("Waiting for your opponent move... ")
            } else if (state.turn == State.Turn.WHITEWIN && player == State.Turn.WHITE) {
                println("YOU LOSE!")
                exitProcess(0)
            } else if (state.turn == State.Turn.BLACKWIN && player == State.Turn.BLACK ) {
                println("YOU WIN!")
                exitProcess(0)
            } else if (state.turn == State.Turn.DRAW) {
                println("DRAW!")
                exitProcess(0)
            }
        }
    }

    private fun minMax(state: State, depth: Int, alpha: Float, beta: Float, maxPlayer: Boolean): Float{
        if(depth == 0 || state.turn.toString() == BLACK_WIN || state.turn.toString() == WHITE_WIN|| state.turn.toString() == DRAW){
            return evalPosition(state)
        }
        var currAlpha = alpha
        var currBeta = beta



        if (maxPlayer){

            var goOn = true
            var maxEval = -100F
            val stateIterator = StateIterator(state, mRules)
            while (stateIterator.hasNext() && goOn){
                val child = stateIterator.next()
                val eval = minMax(child.state, depth - 1, currAlpha, currBeta, false)

                if (eval >= maxEval){
                    maxEval = eval
                }
                if (eval >= currAlpha){
                    currAlpha = eval
                    if (depth == MAX_DEPTH){
                        mNextAction = child.action
                        if (eval == 1.0F){
                            println("Best move is $mNextAction")
                        }
                    }
                }
                if (currBeta <= currAlpha){
                    goOn = false
                }
            }

            return  maxEval
        }else{
            var minEval = 100F
            var goOn = true
            val stateIterator = StateIterator(state, mRules)
            while (stateIterator.hasNext() && goOn) {
                val child = stateIterator.next()
                val eval = minMax(child.state, depth - 1, currAlpha, currBeta, true)
                minEval = Math.min(minEval, eval)
                if (eval < currBeta){
                    currBeta = eval
                }
                if (currBeta <= currAlpha){
                    goOn = false
                }
            }
            return  minEval
        }
    }

    private fun evalPosition(state: State): Float {
        if ((player.toString() == WHITE && state.turn.toString() == WHITE_WIN) || (player.toString() == BLACK && state.turn.toString() == BLACK_WIN))
            return 1F
        if ((player.toString() == BLACK && state.turn.toString() == WHITE_WIN) || (player.toString() == WHITE && state.turn.toString() == BLACK_WIN))
            return -1F
        if (state.turn.toString() == DRAW)
            return  0F


        val cornersScores = Array<AnalysisResult?>(4){null}
        for (i in 0 .. 3){
            cornersScores[i] = cornerAnalysis(state, i)
        }

        mNBlacks = 0
        mNWhites =0
        kinkJ = -1
        kinkI = -1
        val relAnal1 = relationshipAnalysis(state, true)
        val relAnal2 = relationshipAnalysis(state, false)
        val relAnalysisResult = AnalysisResult((relAnal1.whiteScore + relAnal2.whiteScore)/2, (relAnal1.blackScore + relAnal2.blackScore)/2, (relAnal1.kingScore + relAnal2.kingScore)/2 )

        val a = 1
        val b = 4
        val c = 1
        val d = 3
        val e = 1
        val f = 2
        val g = 1


        val relKing = if (relAnalysisResult.kingScore > 20) 20 else relAnalysisResult.kingScore
        val relWhite = if (relAnalysisResult.whiteScore > mNWhites*20) mNWhites*20 else relAnalysisResult.whiteScore
        val relBlack = if (relAnalysisResult.blackScore > mNBlacks*20) mNBlacks*20 else relAnalysisResult.blackScore

        val finalScoreWhites : Float = (a*(cornersScores[0]!!.whiteScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat() + cornersScores[1]!!.whiteScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat() + cornersScores[2]!!.whiteScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat() + cornersScores[3]!!.whiteScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat()) +
                                        b*mNWhites/8.0 +
                                        c*relWhite/(mNWhites*20.0) +
                                        d*relKing/20.0).toFloat()


        val finalBlackScore: Float =   (e*(cornersScores[0]!!.blackScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat() + cornersScores[1]!!.blackScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat() + cornersScores[2]!!.blackScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat() + cornersScores[3]!!.blackScore/ AnalysisResult.MAX_ANALYSIS_RES.toFloat()) +
                                        f*mNBlacks/12.0 +
                                        g*relBlack/(mNWhites*20.0)).toFloat()





        var finalScore = finalScoreWhites/(a+b+c+d) - finalBlackScore/(e+f+g)

        if (player.toString() == BLACK)
            finalScore = - finalScore

        return finalScore

    }

    private fun relationshipAnalysis(state: State, leftToRight: Boolean): AnalysisResult {
        val iS = if (leftToRight) 0 until boardSize else boardSize-1 downTo 0
        val jS = if (leftToRight) 0 until boardSize else boardSize-1 downTo 0
        val direction = if (leftToRight) 1 else -1



        val closestTopWhite = arrayListOf(-1, -1, -1, -1, -1, -1, -1, -1, -1)
        val closestTopBlack = arrayListOf(-1, -1, -1, -1, -1, -1, -1, -1, -1)
        val closestLeftWhite = arrayListOf(-1, -1, -1, -1, -1, -1, -1, -1, -1)
        val closestLeftBlack = arrayListOf(-1, -1, -1, -1, -1, -1, -1, -1, -1)

        val scoreArray = Array(9){IntArray(9){0} }
        var kingScore = 0

        for(i in iS){
            val redI = if(i>=4) 8-i else i

            for (j in jS){
                val redJ = if(j>4) 8-j else j
                var potentialLeftWhiteAttack = -1
                var potentialLeftBlackAttack = -1
                val pawn = state.getPawn(i,j)
                when(pawn.toString()){
                    WHITE-> {
                        if (leftToRight)
                           mNWhites++
                        scoreArray[i][j] = 1 // possibly under attack

                        // if there is something on the left
                        if (closestLeftWhite[i] != -1 && ((leftToRight && closestLeftWhite[i] > closestLeftBlack[i] ) || (!leftToRight && closestLeftWhite[i] < closestLeftBlack[i] ))) {
                            if (notCrossesFort(i, closestLeftWhite[i], true, leftToRight, redI)) {
                                // white connection
                                scoreArray[i][j] += 1
                                scoreArray[i][closestLeftWhite[i]] += 1

                            }
                        }else if ((closestLeftBlack[i] in 1..7) && ((leftToRight && closestLeftWhite[i] < closestLeftBlack[i] ) || (!leftToRight && closestLeftWhite[i] > closestLeftBlack[i] )) && closestLeftBlack[i] != j-direction*1 ){
                            if (notCrossesFort(i, closestLeftBlack[i], true, leftToRight, redI) && closestLeftBlack[i] !=0 ){
                                val leftToBlackPawn = state.getPawn(i, closestLeftBlack[i] - direction*1 )
                                // if left to BLACK there is WHITE
                                if (leftToBlackPawn.toString() == WHITE ){
                                    scoreArray[i][j] += 3
                                    scoreArray[i][closestLeftBlack[i]-direction] += 3
                                }
                                if (leftToBlackPawn.toString() == KING ){
                                    scoreArray[i][j] += 4
                                    scoreArray[i][closestLeftBlack[i]-direction] += 2
                                }
                            }
                        }
                        // if there is something on top
                        if (closestTopWhite[i] != -1 && ((leftToRight && closestTopWhite[j] > closestTopBlack[j] ) || (!leftToRight && closestTopWhite[j] < closestTopBlack[j] ))) {
                            if (notCrossesFort(j, closestTopWhite[j], true, leftToRight, redJ)) {

                                // white connection
                                scoreArray[i][j] += 1
                                scoreArray[i][closestTopWhite[i]] += 1

                            }
                        }else if ((closestTopBlack[j] in 1..7) && ((leftToRight && closestTopWhite[j] < closestTopBlack[j] ) || (!leftToRight && closestTopWhite[j] > closestTopBlack[j] )) && closestLeftBlack[i] != j-1 ){
                            if (notCrossesFort(j, closestTopBlack[j], true, leftToRight, redJ) && closestLeftBlack[i] !=0 ){
                                val topToBlackPawn = state.getPawn(i, closestTopBlack[j] - 1 )
                                // potential horizontal attacker
                                    if (topToBlackPawn.toString() == WHITE ){
                                        scoreArray[i][j] += 3
                                        scoreArray[closestTopBlack[j]-direction][j] += 3
                                    }

                                    if (topToBlackPawn.toString() == KING ){
                                        scoreArray[i][j] += 4
                                        scoreArray[closestTopBlack[j]-direction][j] += 2
                                    }
                                }
                            }

                        // potential vertical attacker attacker
                        if (potentialLeftWhiteAttack != -1 && redI>1){
                            val  topToBlackPawn = state.getPawn(i-direction*2, potentialLeftWhiteAttack)
                            if (topToBlackPawn.toString() == WHITE ){
                                scoreArray[i][j] += 3
                                scoreArray[i-direction*2][potentialLeftWhiteAttack] += 3
                            }else

                            if (topToBlackPawn.toString() == KING ){
                                scoreArray[i][j] += 4
                                scoreArray[i-direction*2][potentialLeftWhiteAttack] += 2
                            }else
                                if (i-direction*2==4 && potentialLeftWhiteAttack == 4  ){
                                    //throne
                                    scoreArray[i][j] += 3
                                    scoreArray[i-direction*2][potentialLeftWhiteAttack] += 3
                                }
                        }


                        //look for potential horizontal attack
                        if ((leftToRight && i>0 && j>=2) || (!leftToRight && i<8 && j<=6)){
                            var nextRow : Int
                            var k = 1
                            do {
                                nextRow = i - direction*k
                                if (!((j == 4 && (nextRow == 2 || nextRow == 4 || nextRow == 6 )) && k!=1)){
                                    val leftFig = state.getPawn(nextRow, j-direction)
                                    if (leftFig.toString() == BLACK){
                                        val leftToBlack = state.getPawn(nextRow, j-direction*2)
                                        // if left to BLACK there is WHITE
                                        if (leftToBlack.toString() == WHITE ){
                                            scoreArray[i][j] += 3
                                            scoreArray[nextRow][j-direction*2] += 3
                                            break
                                        }else
                                        if (leftToBlack.toString() == KING ){
                                            scoreArray[i][j] += 4
                                            scoreArray[nextRow][j-direction*2] += 2
                                            break
                                        }else
                                        if (nextRow==4 && j-direction*2 == 4  ){
                                            //throne
                                            scoreArray[i][j] += 3
                                            scoreArray[nextRow][j-direction*2] += 3
                                            break
                                        }

                                    }
                                }
                                k++
                            }while(nextRow != closestTopWhite[j] && nextRow != closestTopBlack[j] && nextRow > 0 && nextRow < boardSize - 1 && (redJ!=1 || (redJ==1 && nextRow!=4)) )
                        }
                        closestLeftWhite[j] = i
                        closestTopWhite[i] = j
                    }
                    BLACK -> {
                        if (leftToRight)
                            mNBlacks++
                        scoreArray[i][j] = -1 // possibly under attack

                        // if there is something on the left
                        if (closestLeftBlack[i] != -1 && ((leftToRight && closestLeftWhite[i] < closestLeftBlack[i] ) || (!leftToRight && closestLeftWhite[i] > closestLeftBlack[i] ))) {
                            if (notCrossesFort(i, closestLeftBlack[i], false, leftToRight, redI)) {

                                // black connection
                                scoreArray[i][j] -= 1
                                scoreArray[i][closestLeftBlack[i]] -= 1

                            }
                        }else if ((closestLeftWhite[i] in 1..7) && ((leftToRight && closestLeftWhite[i] > closestLeftBlack[i] ) || (!leftToRight && closestLeftWhite[i] < closestLeftBlack[i] )) && closestLeftWhite[i] != j-direction*1){
                            if (notCrossesFort(i, closestLeftWhite[i], false, leftToRight, redI) && closestLeftWhite[i] !=0 ){
                                val leftToWhiteInd = closestLeftWhite[i] - direction
                                val leftToWhitePawn = state.getPawn(i, leftToWhiteInd )
                                val closestWhite = state.getPawn(i, closestLeftWhite[i])
                                // if left to White there is danger
                                if (leftToWhitePawn.toString() == BLACK || isNearFort(i, closestLeftWhite[i])){
                                    if (closestWhite.toString() == WHITE){
                                        scoreArray[i][j] -= 3
                                        scoreArray[i][leftToWhiteInd] -= 3
                                    }else
                                    if (closestWhite.toString() == KING){
                                        scoreArray[i][j] -= 2
                                        scoreArray[i][leftToWhiteInd] -= 4
                                    }else
                                    if(i == 4 && leftToWhiteInd == 4){
                                        scoreArray[i][j] -= 3
                                    }

                                }
                            }
                        }
                        // if there is something on the top
                        if (closestTopBlack[j] != -1 && ((leftToRight && closestTopWhite[i] < closestTopBlack[i] ) || (!leftToRight && closestTopWhite[i] > closestTopBlack[i] ))) {
                            if (notCrossesFort(j, closestTopBlack[j], false, leftToRight, redJ)) {

                                // black connection
                                scoreArray[i][j] -= 1
                                scoreArray[closestTopBlack[j]][j] -= 1

                            }
                        }else if ((closestTopWhite[j] in 1..7) && ((leftToRight && closestLeftWhite[i] > closestLeftBlack[i] ) || (!leftToRight && closestLeftWhite[i] < closestLeftBlack[i] )) && closestLeftWhite[i] != j-direction*1){
                            if (notCrossesFort(j, closestTopWhite[j], false, leftToRight, redJ) && closestTopWhite[i] !=0 ){
                                val topToWhiteInd = closestTopWhite[j] - direction
                                val topToWhitePawn = state.getPawn(topToWhiteInd, j )
                                val closestWhite = state.getPawn(closestTopWhite[j], j)
                                // if top to White there is danger
                                if (topToWhitePawn.toString() == BLACK || isNearFort(i, closestTopWhite[j])){
                                    if (closestWhite.toString() == WHITE){
                                        scoreArray[i][j] -= 3
                                        scoreArray[topToWhiteInd][j] -= 3
                                }else
                                    if (closestWhite.toString() == KING){
                                        scoreArray[i][j] -= 2
                                        scoreArray[topToWhiteInd][j] -= 4
                                    }else
                                        if(i == 4 && topToWhiteInd== 4){
                                            scoreArray[i][j] -= 3
                                        }

                                }
                            }
                        }

                        // potential vertical attacker //
                        if (redI > 2){
                            if (potentialLeftBlackAttack != -1){
                                val topToWhitePawn = state.getPawn(i-direction*2, potentialLeftBlackAttack)
                                if (topToWhitePawn.toString() == BLACK ){
                                    scoreArray[i][j] -= 3
                                    scoreArray[i-direction*2][potentialLeftBlackAttack] -= 3
                                }else if (topToWhitePawn.toString() == KING ){
                                    scoreArray[i][j] -= 4
                                    scoreArray[i-direction*2][potentialLeftBlackAttack] -= 2
                                }else if (i-direction*2 == 4 && potentialLeftBlackAttack == 4){
                                    scoreArray[i][j] -= 3
                                }
                            }
                        }



                        //look for potential horizontal attack
                        if (redI>0 && redJ >=2){
                            var nextRow : Int
                            var k = 1
                            do {
                                nextRow = i - direction*k
                                if (!((j == 4 && (nextRow == 2 || nextRow == 4 || nextRow == 6 )) && k!=1)){
                                    val leftFig = state.getPawn(nextRow, j-direction)
                                    if (leftFig.toString() == WHITE || leftFig.toString() == KING){
                                        val leftToBlack = state.getPawn(nextRow, j-2)
                                        // if left to BLACK there is WHITE
                                        if (leftToBlack.toString() == BLACK || isNearFort(nextRow, j-direction) ){
                                            scoreArray[i][j] -= 3
                                            scoreArray[nextRow][j-direction*2] -= 3
                                            break
                                        }else
                                            if (leftToBlack.toString() == KING ){
                                                scoreArray[i][j] -= 2
                                                scoreArray[nextRow][j-direction*2] -= 4
                                                break
                                            }else
                                                if (nextRow==4 && j-direction*2 == 4  ){
                                                    //throne
                                                    scoreArray[i][j] -= 3
                                                    break
                                                }

                                    }
                                }
                                k++
                            }while(nextRow != closestTopWhite[j] && nextRow != closestTopBlack[j] && nextRow > 0 && nextRow < boardSize - 1 && (redJ!=1 || (redJ==1 && nextRow!=4)) )
                        }
                        closestLeftBlack[j] = i
                        closestTopBlack[i] = j
                    }
                    KING -> {
                        if (leftToRight){
                            kinkI = i
                            kinkJ = j
                        }
                        if (closestLeftBlack[i] == -1 && closestLeftWhite[i] == -1)
                            kingScore += 10
                        else if(closestLeftWhite[i] != -1)
                            kingScore += 2

                        if (closestTopBlack[j] == -1 && closestTopWhite[j] == -1)
                            kingScore += 10
                        else if (closestTopWhite[j] != -1)
                            kingScore +=2

                        for (a in 0 until i){
                            if (closestLeftBlack[a] == -1 && closestLeftWhite[a] == -1)
                                kingScore += 2
                        }
                        for (b in 0 until j){
                            if (closestTopBlack[b] == -1 && closestTopWhite[b] == -1)
                                kingScore += 2
                        }
                    }
                    else -> {
                        if ((leftToRight && i>2) || (!leftToRight && i<6)){
                            val fig1 = state.getPawn(i-direction, j)
                            val fig2 = state.getPawn(i-direction*2, j)
                            if (fig1.toString() == BLACK){
                                if (fig2.toString() == WHITE || fig2.toString() == KING || (! fig2.isFigure() && i-direction*2 == 4 && j == 4))
                                    potentialLeftWhiteAttack = j
                            }else if (fig1.toString() == WHITE || fig1.toString() == KING){
                                if (fig2.toString() == BLACK || (! fig2.isFigure() && i-direction*2 == 4 && j == 4) || isNearFort(i-direction, j)){
                                    potentialLeftBlackAttack = j
                                }
                            }

                        }
                    }
                }
            }

        }

        var whiteScore = 0
        var blackScore = 0

        for (i in 0 until boardSize){
            for (j in 0 until boardSize) {
                if (scoreArray[i][j] > 0) whiteScore += scoreArray[i][j]
                if (scoreArray[i][j] < 0) blackScore += scoreArray[i][j]

            }
        }

        return AnalysisResult(whiteScore, blackScore, kingScore)
    }


    private fun notCrossesFort(indA: Int, indB: Int, isWhite: Boolean, leftToRight: Boolean, fortRow: Int): Boolean{
        if (fortRow > 1) return false
        if (leftToRight){
            if (fortRow == 0){
                if (isWhite && (indA<3 || indB>5)) return true
                if (!isWhite && (indA<3 || indB>=3)) return true
            }else{
                if(isWhite &&  (indA<4 || indB>4)) return true
                if (!isWhite && (indA<4 || indB>=4)) return true
            }

        }else{
            if (fortRow == 0) {
                if (isWhite && (indA > 5 || indB < 3)) return true
                if (!isWhite && (indA > 5 || indB <= 5)) return true
            }else{
                if(isWhite &&  (indA>4 || indB<4)) return true
                if (!isWhite && (indA>4 || indB<=4)) return true
            }
        }
        return false

    }

    private fun isNearFort(i: Int, j: Int): Boolean{
        val redI = if(i>=4) 8-i else i
        val redJ = if(j>=4) 8-j else j
        return (redI == 0 && redJ ==2) || (redI == 2 && redJ ==0) || (redI == 1 && redJ ==3) || (redI == 3 && redJ ==1) || (redI == 2 && redJ == 4) || (redI == 4 && redJ == 2)
    }

    private fun isKingOnExit(i: Int, j: Int): Boolean{
        val redI = if(i>=4) 8-i else i
        val redJ = if(j>=4) j-i else i

        return (redI == 0 && (redJ == 1 || redJ==2)) || (redJ == 0 && (redI == 1 || redI==2))
    }

    private fun shouldKingExit(state: State): Action?{
        var kingI : Int = -1
        var kingJ : Int = -1

        for (i in 0..8) {
            for (j in 0..8){
                val fig = state.getPawn(i,j)
                if (fig.toString() == KING){
                    kingI = i
                    kingJ = j
                    break
                }
            }
        }

        var otherFig = false
        for (i in 0 until kingI){
            val fig = state.getPawn(i,kingJ)
            if (fig.isFigure()){
                otherFig = true
                break
            }
        }
        if (!otherFig)
            return Action(state.getBox(kingI,kingJ), state.getBox(0, kingJ), state.turn)

        for (i in kingI+1 until 9){
            val fig = state.getPawn(i,kingJ)
            if (fig.isFigure()){
                otherFig = true
                break
            }
        }
        if (!otherFig)
            return Action(state.getBox(kingI,kingJ), state.getBox(8, kingJ), state.turn)

        for (j in 0 until kingJ){
            val fig = state.getPawn(kingI, j)
            if (fig.isFigure()){
                otherFig = true
                break
            }
        }
        if (!otherFig)
            return Action(state.getBox(kingI,kingJ), state.getBox(kingI, 0), state.turn)

        for (j in kingJ+1 until 9){
            val fig = state.getPawn(kingI, j)
            if (fig.isFigure()){
                otherFig = true
                break
            }
        }
        if (!otherFig)
            return Action(state.getBox(kingI,kingJ), state.getBox(kingI, 8), state.turn)

        return null
    }
}





