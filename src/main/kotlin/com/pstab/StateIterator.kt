package com.pstab

import it.unibo.ai.didattica.competition.tablut.domain.Action
import it.unibo.ai.didattica.competition.tablut.domain.Game
import it.unibo.ai.didattica.competition.tablut.domain.State

class StateIterator(private val state: State, private val rules: Game) : Iterator<StateExplorationResult>{
    private val boardSize = state.board.size
    var lastI = 0
    var lastJ = 0
    var nextStateExplorationResult : StateExplorationResult? = null
    var usedAction = mutableListOf<Action>()

    override fun hasNext(): Boolean {
        if (lastI > boardSize || lastJ > boardSize) {
            println("last indexes is reached $lastI | $lastJ")
            return false
        }
        for (i in lastI until  boardSize){
            for (j in lastJ until boardSize){
                val figure = state.getPawn(i, j)
                // add every permitted move
                if (figure.isFigure() &&
                        (figure.equalsPawn(state.turn.toString())  || (state.turn.equalsTurn(State.Turn.WHITE.toString()) && figure.equalsPawn(
                                State.Pawn.KING.toString())))) {
                    var k = 1
                    var nextPos: Int
                    do {
                        nextPos = i - k
                        if (nextPos >= 0){
                            val action = Action(state.getBox(i, j), state.getBox(nextPos, j), state.turn)
                            if(!usedAction.contains(action) && rules.isActionLegal(state, action)) {

                                val newChild = rules.applyAction(state, action)
                                nextStateExplorationResult = StateExplorationResult(action, newChild)
                                usedAction.add(action)
                                lastI = i
                                lastJ = j
                                return true
                            }
                        }
                        k++
                    }while (nextPos >= 0)
                    k = 1
                    do {
                        nextPos = i + k
                        if (nextPos < boardSize){
                            val action = Action(state.getBox(i, j), state.getBox(nextPos, j), state.turn)
                            if(!usedAction.contains(action) && rules.isActionLegal(state, action)) {
                                val newChild = rules.applyAction(state, action)
                                nextStateExplorationResult = StateExplorationResult(action, newChild)
                                usedAction.add(action)
                                lastI = i
                                lastJ = j
                                return true
                            }
                        }
                        k++
                    }while (nextPos < boardSize)

                    // check horizontal
                    k = 1
                    do {
                        nextPos = j - k
                        if (nextPos >= 0){
                            val action = Action(state.getBox(i, j), state.getBox(i, nextPos), state.turn)
                            if(!usedAction.contains(action) && rules.isActionLegal(state, action)) {
                                val newChild = rules.applyAction(state, action)
                                nextStateExplorationResult = StateExplorationResult(action, newChild)
                                usedAction.add(action)
                                lastI = i
                                lastJ = j
                                return true
                            }
                        }
                        k++
                    }while (nextPos >= 0)
                    k = 1
                    do {
                        nextPos = j + k
                        if (nextPos < boardSize){
                            val action = Action(state.getBox(i, j), state.getBox(i, nextPos), state.turn)
                            if(!usedAction.contains(action) && rules.isActionLegal(state, action)) {
                                val newChild = rules.applyAction(state, action)
                                nextStateExplorationResult = StateExplorationResult(action, newChild)
                                usedAction.add(action)
                                lastI = i
                                lastJ = j
                                return true
                            }
                        }
                        k++
                    }while (nextPos < boardSize)
                }
            }
            lastJ = 0
        }
        lastI = 100
        lastJ = 100
        nextStateExplorationResult = null
        return false
    }

    override fun next(): StateExplorationResult {
        check (nextStateExplorationResult != null) {"com.pstab.StateIterator: next action in null"}
        return nextStateExplorationResult!!
    }

}