package com.pstab

class PSTab {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            if (args.contains("-p")) {
                val pArg = args[1 + args.indexOf("-p")]
                check(pArg == "W" || pArg=="B") {"Error: -p value should be W or B, not $pArg"}
                val host = if(args.contains("-h")) args[1 + args.indexOf("-h")] else "localhost"
                val t = if(args.contains("-t")) args[1 + args.indexOf("-t")].toInt() else -1
                val timeout: Int = t
                val player = if(pArg=="W") "WHITE" else "BLACK"
                val myClient = if (timeout == -1) MyTablutClient(player,  ipAddress = host) else
                                    MyTablutClient(player, timeout=timeout,  ipAddress = if (host!="") host else null)
                myClient.run()
            }else{
                println("Error: -p [W|B] argument is required")
            }
        }
    }


}
