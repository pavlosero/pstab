# PSTab Tablut Client

Hi my name is Pavlo Seroyizhko and this is my Tablut Client for Fundamentals of Artificial Intelligence and
Knowledge Representation Tablut Compettion

## Execution
Run the virtual machine
```bash
cd Documents/pstab
java -jar PSTab.jar -p <W|B> [-h <ip address>] 
```

-p accept only W(for white player) or B(for black player) 


-h not mandatory. Client connects to the localhost if not indicated

## Requirements
java version >= 1.8